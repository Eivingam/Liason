library IEEE;
use IEEE.std_logic_1164.all;


entity Liaison is
  port(
       clk : in STD_LOGIC;
       di_ready : in STD_LOGIC;
       reset : in STD_LOGIC;
       mp_data : in STD_LOGIC_VECTOR(3 downto 0);
       do_ready : out STD_LOGIC;
       voted_data : out STD_LOGIC
  );
end Liaison;

architecture Liaison of Liaison is

component bit8voter
  port (
       a : in STD_LOGIC;
       b : in STD_LOGIC;
       c : in STD_LOGIC;
       clk : in STD_LOGIC;
       d : in STD_LOGIC;
       data_out : in STD_LOGIC_VECTOR(3 downto 0);
       di_ready : in STD_LOGIC;
       reset : in STD_LOGIC;
       data_in : out STD_LOGIC_VECTOR(10 downto 0);
       do_ready : out STD_LOGIC;
       voted_data : out STD_LOGIC
  );
end component;
component hamming_enc_11
  port (
       data_in : in STD_LOGIC_VECTOR(10 downto 0);
       data_out : out STD_LOGIC_VECTOR(3 downto 0)
  );
end component;

signal data_out : STD_LOGIC_VECTOR (3 downto 0);
signal data_in : STD_LOGIC_VECTOR (10 downto 0);

begin

U1 : bit8voter
  port map(
       a => mp_data(3),
       b => mp_data(2),
       c => mp_data(1),
       clk => clk,
       d => mp_data(0),
       data_in => data_in,
       data_out => data_out,
       di_ready => di_ready,
       do_ready => do_ready,
       reset => reset,
       voted_data => voted_data
  );

U2 : hamming_enc_11
  port map(
       data_in => data_in,
       data_out => data_out
  );
end Liaison;
