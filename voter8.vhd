library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity bit8voter is
	Port ( 
		clk : in std_logic;
		reset : in std_logic;
		a : in std_logic;
		b : in std_logic;
		c : in std_logic;
		d : in std_logic;
		data_out : in std_logic_vector (3 downto 0);
		di_ready : in std_logic;
		do_ready : out std_logic;
		voted_data : out std_logic;
		data_in : out std_logic_vector(10 downto 0)
		);
end bit8voter;

architecture behavioral of bit8voter is
	signal yout : std_logic;
	signal abcd : std_logic_vector(3 downto	0);
	signal status : std_logic_vector(2 downto 0);
begin
	yout <= (((c or abcd(1))and(d or abcd(0)))and((b or abcd(2)) or 
	(a or abcd(3)))) or (((a or abcd(3))and(b or abcd(2)))and((d or abcd(0)) or (c or abcd(1))));
	process(abcd)
	begin
		case abcd is
			when "0000"	=> status <="000";
			when "0001"|"0010"|"0100"|"1000" => status <="001";
			when "0011"|"0101"|"0110"|"1001"|"1010"|"1100" => status <= "010";
			when others => status <="111";
		end case;
	end process;
	process(clk)
		variable counter : integer range 0 to 14 := 0;
		variable y_reg : std_logic_vector(10 downto 3);
	begin
		if(rising_edge(clk)) then
			if(reset='1') then
				abcd <="0000";
				counter:=14;
				do_ready<='0';
				voted_data <='0';
			elsif(di_ready='1') then
				do_ready<='1';
				voted_data<=yout;
				y_reg(10) := yout;
				counter:=0;
				if(abcd(3)='0') then
					abcd(3)<=(yout xor a);
				end if;	
				if(abcd(2)='0') then
					abcd(2)<=(yout xor b);
				end if;	
				if(abcd(1)='0') then
					abcd(1)<=(yout xor c);
				end if;	
				if(abcd(0)='0') then
					abcd(0)<=(yout xor d);
				end if;
			elsif(counter<7) then
				do_ready<='0';
				counter := counter + 1;
				voted_data<=yout;
				y_reg(10-counter) := yout;
				if(abcd(3)='0') then
					abcd(3)<=(yout xor a);
				end if;	
				if(abcd(2)='0') then
					abcd(2)<=(yout xor b);
				end if;	
				if(abcd(1)='0') then
					abcd(1)<=(yout xor c);
				end if;	
				if(abcd(0)='0') then
					abcd(0)<=(yout xor d);
				end if;
			elsif(counter=7) then	
				counter := counter + 1;
				voted_data<=status(2);
				
				data_in(10) <= y_reg(10);
				data_in(9) <= y_reg(9);
				data_in(8) <= y_reg(8);
				data_in(7) <= y_reg(7);
				data_in(6) <= y_reg(6);
				data_in(5) <= y_reg(5);
				data_in(4) <= y_reg(4);
				data_in(3) <= y_reg(3);
				data_in(2)<=status(2);
				data_in(1)<=status(1);
				data_in(0)<=status(0);
			elsif(counter=8) then	
				counter := counter + 1;
				voted_data<=status(1);
			elsif(counter=9) then	
				counter := counter + 1;
				voted_data<=status(0);
			elsif(counter=10) then	
				counter := counter + 1;
				voted_data<=data_out(3);
			elsif(counter=11) then	
				counter := counter + 1;
				voted_data<=data_out(2);
			elsif(counter=12) then	
				counter := counter + 1;
				voted_data<=data_out(1);
			elsif(counter=13) then	
				counter := 14;
				voted_data<=data_out(0);
			else
				voted_data<='0';
			end if;
		end if;
	end process;
end architecture;