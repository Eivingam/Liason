library ieee;
use ieee.std_logic_1164.all;

entity liaison_tb is
end liaison_tb;

architecture TB_ARCHITECTURE of liaison_tb is
	component liaison
	port(
		clk : in STD_LOGIC;
		di_ready : in STD_LOGIC;
		reset : in STD_LOGIC;
		mp_data : in STD_LOGIC_VECTOR(3 downto 0);
		do_ready : out STD_LOGIC;
		voted_data : out STD_LOGIC );
	end component;

	signal clk : STD_LOGIC;
	signal di_ready : STD_LOGIC;
	signal reset : STD_LOGIC;
	signal mp_data : STD_LOGIC_VECTOR(3 downto 0);
	signal do_ready : STD_LOGIC;
	signal voted_data : STD_LOGIC;

begin
	UUT : liaison
		port map (
			clk => clk,
			di_ready => di_ready,
			reset => reset,
			mp_data => mp_data,
			do_ready => do_ready,
			voted_data => voted_data
		);

STIMULUS: process -- run for 14ms
begin 
	di_ready <= '0';
	reset <= '1';
	clk <= '0';
	mp_data <= "1111";
    wait for 500 us; --0 fs
	clk <= '1';
    wait for 500 us; --500 us
	reset <= '0';
	clk <= '0';
    wait for 500 us; --1 ms
	clk <= '1';
    wait for 500 us; --1500 us
	clk <= '0';
    wait for 500 us; --2 ms
	clk <= '1';
    wait for 500 us; --2500 us
	clk <= '0';
    wait for 500 us; --3 ms
	clk <= '1';
    wait for 500 us; --3500 us
	di_ready <= '1';
	reset <= '1';
	clk <= '0';
    wait for 500 us; --4 ms
	clk <= '1';
    wait for 500 us; --4500 us
	di_ready <= '0';
	reset <= '0';
	clk <= '0';
    wait for 500 us; --5 ms
	clk <= '1';
    wait for 500 us; --5500 us
	clk <= '0';
    wait for 500 us; --6 ms
	clk <= '1';
    wait for 500 us; --6500 us
	clk <= '0';
    wait for 500 us; --7 ms
	clk <= '1';
    wait for 500 us; --7500 us
	clk <= '0';
    wait for 500 us; --8 ms
	clk <= '1';
    wait for 500 us; --8500 us
	clk <= '0';
    wait for 500 us; --9 ms
	clk <= '1';
    wait for 500 us; --9500 us
	di_ready <= '1';
	clk <= '0';
    wait for 500 us; --10 ms
	clk <= '1';
    wait for 500 us; --10500 us
	di_ready <= '0';
	clk <= '0';
    wait for 500 us; --11 ms
	clk <= '1';
    wait for 500 us; --11500 us
	clk <= '0';
    wait for 500 us; --12 ms
	clk <= '1';
    wait for 500 us; --12500 us
	clk <= '0';
    wait for 500 us; --13 ms
	clk <= '1';
    wait for 500 us; --13500 us
	clk <= '0';
	wait;
end process;

end TB_ARCHITECTURE;

configuration TESTBENCH_FOR_liaison of liaison_tb is
	for TB_ARCHITECTURE
		for UUT : liaison
			use entity work.liaison(liaison);
		end for;
	end for;
end TESTBENCH_FOR_liaison;