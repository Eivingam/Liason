library ieee;
use ieee.std_logic_1164.all;

entity testbench2 is
end testbench2;

architecture testbench2 of testbench2 is
	signal clk : STD_LOGIC;
	signal di_ready : STD_LOGIC;
	signal reset : STD_LOGIC;
	signal mp_data : STD_LOGIC_VECTOR(3 downto 0);
	signal do_ready : STD_LOGIC;
	signal voted_data : STD_LOGIC;
	
	type testvec_t is array(0 to 1, 0 to 29) of std_logic_vector(6 downto 0);
	
	constant testvec : testvec_t := ((
		-- di_ready & mp_data  & voted_data & do_ready
		0 => "1"	& "1111"   & 	"1" 	&  "1",
		1 => "0"	& "0000"   & 	"0" 	&  "0",
		2 => "0"	& "1111"   & 	"1"		&  "0",
		3 => "0"	& "0000"   & 	"0" 	&  "0",	
		4 => "0"	& "1111"   & 	"1" 	&  "0",
		5 => "0"	& "0000"   & 	"0" 	&  "0",
		6 => "0"	& "1111"   & 	"1" 	&  "0",
		7 => "0"	& "0000"   & 	"0" 	&  "0",
		8 => "0"	& "1010"   & 	"0" 	&  "0", 	 -- Intentionally, not a fault
		9 => "0"	& "1010"   & 	"0" 	&  "0",
		10 => "0"	& "1010"   & 	"0" 	&  "0",
		11 => "0"	& "1010"   & 	"0" 	&  "0",
		12 => "0"	& "1010"   & 	"0" 	&  "0",
		13 => "0"	& "1010"   & 	"0" 	&  "0",
		14 => "0"	& "1010"   & 	"0" 	&  "0",		
		15 => "1"	& "1111"   & 	"1" 	&  "1",
		16 => "0"	& "0000"   & 	"0" 	&  "0",
		17 => "0"	& "1111"   & 	"1" 	&  "0",
		18 => "0"	& "0000"   & 	"0" 	&  "0",
		19 => "0"	& "1111"   & 	"1" 	&  "0",
		20 => "0"	& "0000"   & 	"0" 	&  "0",
		21 => "0"	& "1111"   & 	"1" 	&  "0",
		22 => "0"	& "0000"   & 	"0" 	&  "0",
		23 => "0"	& "1111"   & 	"0" 	&  "0",
		24 => "0"	& "0000"   & 	"0" 	&  "0",
		25 => "0"	& "1111"   & 	"0" 	&  "0",
		26 => "0"	& "0000"   & 	"0" 	&  "0",
		27 => "0"	& "1111"   & 	"0" 	&  "0",
		28 => "0"	& "0000"   & 	"0" 	&  "0",
		29 => "0"	& "0000"   & 	"0" 	&  "0"
	),(
		--di_ready & mp_data  & voted_data & do_ready
		0 => "1"	& "1111"   & 	"1" 	&  "1",
		1 => "0"	& "0000"   & 	"0" 	&  "0",
		2 => "0"	& "0000"   & 	"0"		&  "0",
		3 => "0"	& "1111"   & 	"1" 	&  "0",	
		4 => "0"	& "0000"   & 	"0" 	&  "0",
		5 => "0"	& "1111"   & 	"1" 	&  "0",
		6 => "0"	& "0000"   & 	"0" 	&  "0",
		7 => "0"	& "0001"   & 	"0" 	&  "0",		 
		8 => "0"	& "1010"   & 	"0" 	&  "0", 	  
		9 => "0"	& "1010"   & 	"0" 	&  "0",
		10 => "0"	& "1010"   & 	"1" 	&  "0",
		11 => "0"	& "1010"   & 	"1" 	&  "0",
		12 => "0"	& "1010"   & 	"0" 	&  "0",
		13 => "0"	& "1010"   & 	"1" 	&  "0",
		14 => "0"	& "1010"   & 	"0" 	&  "0",
		15 => "1"	& "1111"   & 	"1" 	&  "1",
		16 => "0"	& "0000"   & 	"0" 	&  "0",
		17 => "0"	& "1111"   & 	"1" 	&  "0",
		18 => "0"	& "0000"   & 	"0" 	&  "0",
		19 => "0"	& "1111"   & 	"1" 	&  "0",
		20 => "0"	& "0000"   & 	"0" 	&  "0",
		21 => "0"	& "1111"   & 	"1" 	&  "0",
		22 => "0"	& "0000"   & 	"0" 	&  "0",
		23 => "0"	& "1111"   & 	"0" 	&  "0",
		24 => "0"	& "0000"   & 	"0" 	&  "0",
		25 => "0"	& "1111"   & 	"0" 	&  "0",
		26 => "0"	& "0000"   & 	"0" 	&  "0",
		27 => "0"	& "1111"   & 	"0" 	&  "0",
		28 => "0"	& "0000"   & 	"0" 	&  "0",
		29 => "0"	& "0000"   & 	"0" 	&  "0"
	));
	
begin
	-- Change this to your entity and architecture name.
	dut: entity work.Liaison(Liaison)
		port map (
			clk => clk, reset => reset,
			mp_data(3) => mp_data(3), mp_data(2) => mp_data(2), mp_data(1) => mp_data(1), 
			mp_data(0) => mp_data(0), 
			di_ready => di_ready, do_ready => do_ready, voted_data => voted_data
		);
		
	clk <= not clk after 5ns;
	
	process is begin
		for i in testvec'range(1) loop
			-- Reset:
			reset <= '1';
			wait until falling_edge(clk);
			reset <= '0';
			
			for j in testvec'range(2) loop
				-- Update inputs.
				mp_data <= testvec(i, j)(5 downto 2);
				di_ready <= testvec(i, j)(6);
				-- Wait one clock cycle, then check ouputs.
				wait until falling_edge(clk);
				assert voted_data = testvec(i, j)(1) report "Output 'voted_data' wrong at vector " & integer'image(i) & ", " & integer'image(j) severity error;
				assert do_ready = testvec(i, j)(0) report "Output 'status' wrong at vector " & integer'image(i) & ", " & integer'image(j) severity error;
			end loop;
		end loop;
		
		wait; -- Testbench completed.
	end process;
end architecture;